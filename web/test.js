var execCode = Module.cwrap("createCPUforCode", "number", ["string"]);
var delCPU = Module.cwrap("closeCPU", "number", []);
var tickCPU = Module.cwrap("tickCPU", "number", ["number", "number"]);
var peekMem =  Module.cwrap("peekCPUMem", "number", ["number","number"]);

var editor;
var watcherTable;
var cpu = null;

var setup = function()
{
	//--- setup editor ----
	editor = ace.edit("editor");
	editor.setTheme("ace/theme/monokai");
	editor.getSession().setMode("ace/mode/assembly_x86");
	
	editor.on("guttermousedown", function(e){ 
			var target = e.domEvent.target; 
			if (target.className.indexOf("ace_gutter-cell") == -1) 
				return; 
			if (!editor.isFocused()) 
				return; 
			if (e.clientX > 25 + target.getBoundingClientRect().left) 
				return; 

			var row = e.getDocumentPosition().row
			
			console.log(e.editor.session.getBreakpoints());
			if(e.editor.session.getBreakpoints().length > row && e.editor.session.getBreakpoints()[row] == "freyrbp")
				e.editor.session.clearBreakpoint(row);
			else
				e.editor.session.setBreakpoint(row, "freyrbp");
				
			e.stop() 
		}) 
	
	//--setup watcher debugger
	
	watcherTable = document.getElementById("watcher-table");
	
	var names = ["A", "B", "C", "X", "Y", "Z", "I", "J", "SP", "PC", "EX"];
	
	for(var i = 0; i < 11; ++i)
	{
		var row = watcherTable.insertRow(watcherTable.rows.length);
		
		var nameCell = row.insertCell(0);
		var valueCell = row.insertCell(1);
		
		nameCell.innerHTML = names[i];
	}
}

var updateWatcher = function()
{
	for(var i = 0; i < 11; ++i)
	{
		watcherTable.rows[i].cells[1].innerHTML = peekMem(cpu, i);
	}
}

var sendCode = function()
{
	if(cpu != null)
		delCPU(cpu);
		
	var code = editor.getValue();
	cpu = execCode(code);
	tickCPU(cpu, 0.1);
	
	updateWatcher();
}