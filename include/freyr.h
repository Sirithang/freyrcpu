//////////////////////////////////////////////////////////////////////////
// Freyr CPU impl
//////////////////////////////////////////////////////////////////////////
#pragma once

#include <memory.h>

//create a mask from bit s of n length
#define CREATE_MASK(s,n) (((1 << n) - 1) << s);

#ifdef DO_FREYR_ERROR_CHECK
#define FREYR_ERROR(msg) assert(1)
#else
#define  FREYR_ERROR(msg)
#endif

//-----------------

typedef struct
{

	union
	{
		struct 
		{
			//register
			unsigned short A,B,C,X,Y,Z,I,J;
			//pointer
			unsigned short SP, PC;
			//carry register
			unsigned short EX;
			unsigned short RAM[65535];
		};

		unsigned short datas[65535 + 11 * sizeof(unsigned short)];
	} memory;

	//this is increased by op decoding. Once reachin op cost, it actually do the op.
	unsigned char cycleCount;
} CPUState ;

typedef void (*PCHook)(CPUState* cpu);

void freyr_register_hook(PCHook hook);

//init the cpu
void freyr_cpu_init(CPUState* cpu);
void freyr_cpu_tick(CPUState* cpu, float deltaTime);
void freyr_cpu_op(CPUState* cpu, unsigned short op);

//everything modifying the pc should go throught that
//Help debugger by offering a single entry point to PC modification
void freyr_cpu_setPC(CPUState* cpu, unsigned short value);
void freyr_cpu_incPC(CPUState* cpu);

//############### OP FUNC #######################

//This allow to get the index in cpu.memory for given value
int freyr_op_valueToIndex(CPUState* cpu, unsigned short val);

void freyr_op_SET(CPUState* cpu, unsigned short b, unsigned short a);
void freyr_op_ADD(CPUState* cpu, unsigned short b, unsigned short a);

//###############################################

