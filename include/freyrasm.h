//////////////////////////////////////////////////////////////////////////
// Freyr Assembler
//////////////////////////////////////////////////////////////////////////

#pragma once

#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct
{
	char name[16];
	unsigned short val;
} Mnemonic;

const Mnemonic mnemos[] = 
{
	{"SET", 0x1},
	{"ADD", 0x2},
	{"SUB", 0x3}
};
const int mnemocount = sizeof(mnemos)/sizeof(mnemos[0]);

const Mnemonic targets[] = 
{
	{"A", 0x0},
	{"B", 0x1},
	{"C", 0x2},
	{"X", 0x3},
	{"Y", 0x4},
	{"Z", 0x5},
	{"I", 0x6},
	{"J", 0x7},
	{"SP", 0x18},
	{"PC", 0x19},
	{"EX", 0x1a},
};
const int targetcount = sizeof(targets)/sizeof(targets[0]);

typedef struct 
{
	unsigned int beg;
	unsigned int end;
} SubstringData;

int substrsize(SubstringData d)
{
	return d.end - d.beg + 1;
}

void freyrasm_assemble(const char* source, unsigned short* output, unsigned int maxOutput);

//return the opcode corresponding to the dtaa given. if invalid, return 0xffff
unsigned short freyrasm_parse(const char* source, SubstringData mnemo, SubstringData b, SubstringData a);

//###########################################

void freyrasm_assemble(const char* source, unsigned short* output, unsigned int maxOutput)
{
	unsigned int currentOutputIdx = 0;
	unsigned int sourceSize = strlen(source);

	SubstringData datas[3] = { {0,0} , {0,0}, {0,0} };

	//0 mnemonis, 1 b, 2 a
	unsigned int currentState = 0;
	unsigned char started = 0;
	char previous = 0;

	for(unsigned int i = 0; i < sourceSize; ++i)
	{
		int handled = 1;
		switch(source[i])
		{
		case '\n':
			//new line, parse & reset to mnemonic state

			if(started == 1)//was started but yet not finished
				datas[currentState].end = i-1;

			output[currentOutputIdx++] = freyrasm_parse(source, datas[0], datas[1], datas[2]);


			started = 0;
			currentState = 0;

			memset(datas, 0, 3 * sizeof(SubstringData));

			break;
		//both those define end of current data (mnemo or value)
		case ',':
		case ' ':
			if(isalnum(previous))
			{//we ignore multiple space & space following a ,
				datas[currentState].end = i-1;

				currentState += 1;
				started = 0;
			}
			break;
		default:
			if(started == 0 && (isalnum(source[i]) || source[i] == '['))
			{//first alphanum char after finished previous value
				datas[currentState].beg = i;
				started = 1;
			}
			break;
		}
		previous = source[i];
	}

	if(started == 1)
	{
		datas[currentState].end = sourceSize-1;
		output[currentOutputIdx++] = freyrasm_parse(source, datas[0], datas[1], datas[2]);
	}
}

unsigned short freyrasm_parse(const char* source, SubstringData mnemo, SubstringData b, SubstringData a)
{
	unsigned short opcode = 0x0;
	unsigned short bval = 0x0;
	unsigned short aval = 0x0;
	int found = 0;

	char substr[32];

	/*printf("Mnemo found : %*.*s , with b : %*.*s and a : %*.*s \n", 
	substrsize(mnemo), substrsize(mnemo), source + mnemo.beg,
	substrsize(b),substrsize(b), source + b.beg,
	substrsize(a),substrsize(a), source + a.beg);*/


	for(int i = 0; i < mnemocount; ++i)
	{
		if(strncmp(mnemos[i].name, source + mnemo.beg, substrsize(mnemo)) == 0)
		{
			opcode = mnemos[i].val;
			found = 1; 
			break;
		}
	}

	//value of B
	found = 0;
	for(int i = 0; i < targetcount; ++i)
	{
		if(strncmp(targets[i].name, source + b.beg, substrsize(b)) == 0)
		{
			bval = targets[i].val;
			found = 1;
			break;
		}
	}

	if(!found)
	{
		memcpy(substr, source + b.beg, substrsize(b));
		substr[substrsize(b)] = '\0';
		bval = atoi(substr);
	}

	//value of a
	found = 0;
	for(int i = 0; i < targetcount; ++i)
	{
		if(strncmp(targets[i].name, source + a.beg, substrsize(a)) == 0)
		{
			aval = targets[i].val;
			found = 1;
			break;
		}
	}

	if(!found)
	{
		memcpy(substr, source + a.beg, substrsize(a));
		substr[substrsize(a)] = '\0';
		aval = 0x1d + atoi(substr);
	}

	unsigned short result = (aval<<10) | (bval << 5) | (opcode);

	//printf("result : %x \n", result);

	return result;
}