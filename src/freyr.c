#include "freyr.h"
#include <stdio.h>

//close to Snes speed (3.8 MHz)
#define CYCLESPERSECONDS 3500000

PCHook hooks[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void freyr_register_hook(PCHook hook)
{
	int registerPos = 0;
	while(registerPos < 16 && hooks[registerPos] == 0)
	{
		registerPos += 1;
	}

	if(registerPos == 16)
		return;

	hooks[registerPos] = hook;
}

void freyr_cpu_init(CPUState* cpu)
{
	memset(cpu, 0, sizeof(CPUState));

	//reverse stack
	cpu->memory.SP = 0xffff;
}

//tick the cpu, spending one cycle.
//op take different cycles count to complete. once completed, it advance PC.
void freyr_cpu_tick(CPUState* cpu, float deltaTime)
{
	int nbCycle = deltaTime * CYCLESPERSECONDS;

	while(nbCycle > 0 && cpu->memory.RAM[cpu->memory.PC] != 0x0)
	{
		freyr_cpu_op(cpu, cpu->memory.RAM[cpu->memory.PC]);

		nbCycle -= 1;
	}
}

void freyr_cpu_op(CPUState* cpu, unsigned short op)
{
	unsigned short decoded = op & CREATE_MASK(0,5);
	unsigned short b = op & CREATE_MASK(5,5);
	unsigned short a = op & CREATE_MASK(10,6);

	a = a >> 10;
	b = b >> 5;

	printf("op : %x, a : %x, b : %x \n", decoded, a, b);

	switch(decoded)
	{
	case 0x1: // SET op
		freyr_op_SET(cpu, b, a);
		break;
	case 0x2: // ADD op
		freyr_op_ADD(cpu, b, a);
		break;
	default:
		FREYR_ERROR("unrocognised operand");
		break;
	}
}

void freyr_cpu_setPC(CPUState* cpu, unsigned short value)
{
	cpu->memory.PC = value;
}

void freyr_cpu_incPC(CPUState* cpu)
{
	freyr_cpu_setPC(cpu, cpu->memory.PC + 1);
}

//--------------------------------------------

int freyr_op_valueToIndex(CPUState* cpu, unsigned short val)
{
	int index  = -1;

	if(val <= 0x07)
	{
		index = val;
	}
	else if(val <= 0x0f)
	{
		index = cpu->memory.datas[val-0x08];
	}
	else if(val <= 0x17)
	{
		//todo
	}
	else if(val <= 0x1a)
	{
		index = val - 0x10;
	}
	else if(val <= 0x1b)
	{
		//todo, 32bit indexing
	}
	else if(val <= 0x1c)
	{
		//todo, 32bit literal
	}
	else if(val <= 0x1d)
	{//literal value, op will check for special value
		index = -1;
	}

	return index;
}

//--------------------------------------------

void freyr_op_SET(CPUState* cpu, unsigned short b, unsigned short a)
{
	int bidx = freyr_op_valueToIndex(cpu, b);
	int aidx = freyr_op_valueToIndex(cpu, a);

	int value;

	if(aidx == -1)
	{//if == -1 this is a literal
		value = a == 0xffff? -1 : a - 0x1d;
	}
	else
	{//need to read in memory
		value = cpu->memory.datas[aidx];
	}

	printf("value : %i, bidx : %i \n", value, bidx);

	if(bidx != -1)
		cpu->memory.datas[bidx] = value;

	freyr_cpu_incPC(cpu);
}

//--------------------------------------------

void freyr_op_ADD(CPUState* cpu, unsigned short b, unsigned short a)
{
	int bidx = freyr_op_valueToIndex(cpu, b);
	int aidx = freyr_op_valueToIndex(cpu, a);

	int value;

	if(aidx == -1)
	{//if == -1 this is a literal
		value = a == 0xffff? -1 : a - 0x1d;
	}
	else
	{//need to read in memory
		value = cpu->memory.datas[aidx];
	}

	if(bidx != -1)
		cpu->memory.datas[bidx] += value;

	freyr_cpu_incPC(cpu);
}