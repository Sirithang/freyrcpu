#include "freyrasm.h"
#include "freyr.h"

#include <stdio.h>

#define MAX_PROGRAM_SIZE 0x64000

CPUState* createCPUforCode(const char* code)
{
	printf("Received code : %s", code);

	CPUState* cpu = (CPUState*)malloc(sizeof(CPUState));
	freyr_cpu_init(cpu);

	freyrasm_assemble(code, cpu->memory.RAM, MAX_PROGRAM_SIZE);

	return cpu;
}

void closeCPU(CPUState* cpu)
{
	free(cpu);
}

void tickCPU(CPUState* cpu, float deltaTime)
{
	freyr_cpu_tick(cpu,deltaTime);
}

int peekCPUMem(CPUState* cpu, int index)
{
	return cpu->memory.datas[index];
}