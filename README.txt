FREYR
=====

This is a fun/learning project aiming at creating a virtual CPU w/ ecosystem.

It aim at improve my understanding of CPU assembly & offer a fun platform to learn assembly 

CPU specs are a rough mix of DCPU-16 & Snes Cpu (65c816), all mixed with guesses and trial-errors design process...

NOTE : as the goal is ATM to provide a webpage containing a code editor + output (in order to offer easy access to the system and "modernize" the look&feel of assembly programming) the project compile using Emscripten.

All the CPU (and latter hardware) part are in C. This is interop in javascript to create/tick CPUS/hardware.

Being pure C with no external dependances, all but the main.c should compile with any c compiler though.